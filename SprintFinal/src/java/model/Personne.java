/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import model.Annotations;
import static java.lang.System.out;
import java.util.HashMap;

/**
 *
 * @author Kanto
 */
public class Personne {
    String nom;
    int age;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAge() {
        return age;
    }
    
    @Annotations(method="makaAge")
    public ModelView setAge(int age) {
        ModelView model = new ModelView();
        model = null;
        this.age = age;
        System.out.println("il a 6 ans");
        return model;
    }
    
    @Annotations(method="calc")
    public ModelView calcul()
    {
        int c = 3+2;
        Object o = (Object) c;
        ModelView model = new ModelView();
        model.setUrl("login.jsp");
        HashMap<String,Object> map = new HashMap<String,Object>();
        map.put("nombre", o);
        model.setDonnee(map);
//        System.out.println("calcull "+map.keySet());
        return model;
    }
//    
//    @Annotations(method="text")
//    public ModelView information(Personne p)
//    {
//        ModelView model = new ModelView();
//        String text = "Votre nom est "+p.getNom()+" et vous avez "+p.getAge();
//        Object o = (Object) text;
//        model.setUrl("text.jsp");
//        HashMap<String,Object> map = new HashMap<String,Object>();
//        map.put("text", o);
//        model.setDonnee(map);
//        return model;
//    }
    
   @Annotations(method="test")
    public ModelView test()
    {
        String rep = "salut";
        Object o = (Object) rep;
        ModelView model = new ModelView();
        model.setUrl("test.jsp");
        HashMap<String,Object> map = new HashMap<String,Object>();
        map.put("text", o);
        model.setDonnee(map);
        return model;
    }
}
