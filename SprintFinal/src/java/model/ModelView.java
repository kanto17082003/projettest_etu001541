/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;

/**
 *
 * @author Kanto
 */
public class ModelView {
    String url;
    HashMap<String,Object> donnee;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HashMap<String, Object> getDonnee() {
        return donnee;
    }

    public void setDonnee(HashMap<String, Object> donnee) {
        this.donnee = donnee;
    }
    
}
