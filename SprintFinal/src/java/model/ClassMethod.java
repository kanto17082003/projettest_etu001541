/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.lang.reflect.Method;

/**
 *
 * @author Kanto
 */
public class ClassMethod {
    Class classe;
    Method fonction;

    public Class getClasse() {
        return classe;
    }

    public void setClasse(Class classe) {
        this.classe = classe;
    }

    public Method getFonction() {
        return fonction;
    }

    public void setFonction(Method fonction) {
        this.fonction = fonction;
    }
    
}
